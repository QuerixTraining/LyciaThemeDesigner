##########################################################################
# LyciaThemeDesigner Project                                             #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################
MAIN
	DEFINE f3 STRING, f4 STRING
	
	CALL ui.interface.frontcall("html5","scriptImport",["qx://application/6_simple_css_n_js_app.js"],[])
	OPEN WINDOW w1 WITH FORM '6_simple_css_n_js_app' ATTRIBUTE(BORDER)

	INPUT BY NAME f3,f4
		BEFORE FIELD f3
			MESSAGE "Double-click the text field"
		BEFORE FIELD f4
			MESSAGE "Double-click the text field"
	END INPUT
END MAIN