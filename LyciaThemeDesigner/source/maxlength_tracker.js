/*
 LyciaThemeDesigner Demo Project
 Property of Querix Ltd.
 Copyright (C) 2016  Querix Ltd. All rights reserved.
 This program is free software: you can redistribute it. 
 You may modify this program only using Lycia.

 This program is distributed in the hope that it will be useful,
 but without any warranty; without even the implied warranty of
 merchantability or fitness for a particular purpose.

 Email: info@querix.com
*/
(function(querix){

var maxlengthTrackerStyles = 
'.qx-grid-cell,\n'+
'.qx-aum.qx-maxlength-tracker {\n'+
'	overflow: visible !important;\n'+
'}\n'+
'\n'+
'.qx-aum.qx-maxlength-tracker > .tracker {\n'+
'	position: absolute;\n'+
'	display: none;\n'+
'	align-items: center;\n'+
'	justify-content: center;\n'+
'	top: -1px;\n'+
'	right: 0;\n'+
'	font-size: .7rem;\n'+
'	font-family: monospace;\n'+
'	width: 2rem;\n'+
'	height: 1rem;\n'+
'	z-index: 1;\n'+
'	background-color: rgba(200,200,200,.7) !important;\n'+
'	opacity: .8;\n'+
'	border-radius: 0 0 .5rem .5rem;\n'+
'	border: 1px solid #aaa;\n'+
'	border-top-width: 0;\n'+
'	overflow: hidden;\n'+
'}\n'+
'\n'+
'.qx-aum.qx-maxlength-tracker.qx-has-focus > .tracker {\n'+
'	display: flex;\n'+
'}\n'
;
	
var $ = querix.$;
var api = querix.plugins.api;

var get$ = function(el) {
    try{
        if (el) {
          return el.getWindow().$;
        } else {
          return api.getWindow(api.topWindow()).$;
        }
    } catch (e) {
        if (console) {
          console.error('(Cannot get jQuery object of the top window',e.stack);
        }
        return null;
    }
};

var checkUnset = function(value, defaultValue) {
  if (typeof value === 'object' &&
    (value === querix.reactive.unset || value === querix.reactive.undef)) {
    return defaultValue;
  }
  return value;
}

var setMaxtrackerStyles = function($$) {
	var style = $$('#maxlength-tracker-styles');
	if (style.length == 0) {
		$$('head').append('<style id="maxlength-tracker-styles">'+maxlengthTrackerStyles+'</style>');
	}
}

/*var initMaxtracker = function($$, e) {
    var dom = $$('<div class="tracker" />');
    var $text = e.find('.qx-text:first');
    var maxlength = $text.attr('maxlength');
    if (maxlength) {
    	console.error('~~~maxlength.attr',maxlength);
    	dom.text(maxlength);
    }
    e.addClass("qx-maxlength-tracker").append(dom);
    
    $text.on('keyup',function(ev){
    	var $text = $$(this), val;
    	if (this.tagName === 'INPUT') {
    		val = $text.val();
    	} else {
    		val = $text.text();
    	}
    	var charsLeft = $text.attr('maxlength') - val.length;
		dom.html(charsLeft);        	
    	console.error('~~~keypress',ev,charsLeft,val);
    });
}*/

//querix.plugins.api.registerWrapper('maxlength_tracker',{
querix.plugins.wrappers.maxlength_tracker = {
    wrap: function(childId, e, param) {
        var $$ = get$(e), dom, $text;
        setMaxtrackerStyles($$);
        
        var instance = {
            prop: function(path,v) {
            	if (path === 'MaxLength' && parseInt(v) > 0) {
                	console.error('~~~maxLengthTracker',path,'=',v);
				    dom = $$('<div class="tracker" />');
				    $text = e.find('.qx-text:first');
            		maxlength = v;
            		dom.text(v);
				    $text.on('keyup',function(ev){
				    	var val;
				    	if (this.tagName === 'INPUT') {
				    		val = $text.val();
				    	} else {
				    		val = $text.text();
				    	}
				    	var charsLeft = $text.attr('maxlength') - val.length;
						dom.html(charsLeft);        	
				    	//console.error('~~~keypress',ev,charsLeft,val);
				    });
    				e.addClass("qx-maxlength-tracker").append(dom);
            	}
//                console.error('~~~maxLengthTracker',path,'=',v);
                return true;
            },
            attach: function() { 
                //console.error('~~~maxLengthTracker.attach');
            	return true; 
            },
            remove: function() {
                //console.error('~~~maxLengthTracker.remove');
            }
        };
        
        
        return instance;
    }
}
//) //registerWrapper
;

})(querix);
