##########################################################################
# LyciaThemeDesigner Project                                             #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################
DEFINE s1, s2 STRING
DEFINE s3, s4 CHAR(5)
DEFINE s5, s6 STRING
DEFINE s7, s8 INT
DEFINE s9, s11 STRING
DEFINE s12, s13 STRING
DEFINE s14, s15, s16 STRING
DEFINE s17, s18 STRING
DEFINE errors STRING

MAIN
CALL ui.interface.frontcall("html5","scriptImport",["qx://application/maxlength_tracker.js","nowait"],[])
call apply_theme("7_maxlength_wrapper_demo")

  OPEN WINDOW w1 WITH FORM '7_maxlength_wrapper_demo'  ATTRIBUTE(BORDER)
  
  INPUT s1, s2 from f1, f2
  INPUT s3, s4 from f3, f4
  INPUT s5, s6 from f5, f6
  INPUT s7, s8 from f7, f8
  INPUT s9, s11 from f9, f11
  INPUT s12, s13 from f12, f13
  INPUT s14, s16, s15 from f14, f16, f15
  INPUT s17, s18 from f17, f18
   
	LET errors = ""
	  
	IF s1 != s2 OR s1 != "abcde" THEN
		LET errors = errors.append("Step 1. Max length on form: f1 or f2 has incorrect value. \n")   
  	END IF
  
	IF s3 != s4 OR s1 != "abcde" THEN
		LET errors = errors.append("Step 2. Max length on 4gl:  f3 or f4 has incorrect value. \n")   
  	END IF
  
	IF s5 != s6 OR s5 != "11ab" THEN
		LET errors = errors.append("Step 3. Text picture: f5 or f6 has incorrect value. \n")   
  	END IF
  	
	IF s7 != s8 OR s7 != "79" THEN
		LET errors = errors.append("Step 4. Include: f7 or f8 has incorrect value. \n")   
  	END IF  	  

	IF s9 != s11 OR s9 != "abc" THEN
		LET errors = errors.append("Step 5. Disable field: f9 or f11 has incorrect value. \n")   
  	END IF  	  

	IF s12 != s13 OR s12 != "abcde" THEN
		LET errors = errors.append("Step 6. Filled with text: f12 or f13 has incorrect value. \n")   
  	END IF  	  
	
	IF s14 != s15 OR s14 != s16 OR s14 != "abcde" THEN
		LET errors = errors.append("Step 7. Order in INPUT: f13 or f14 or f15 has incorrect value. \n")   
  	END IF  	  

	IF s16 != s17 OR s18 != "abcde" THEN
		LET errors = errors.append("Step 8. Verify: f16 or f17 has incorrect value. \n")   
  	END IF  	  

  	IF errors == "" OR errors IS NULL THEN 
  		MESSAGE "PASSED"
  	ELSE
  		MESSAGE "FAILED"
  		DISPLAY errors
  	END IF		
  	 
  MENU 
    ON ACTION exit_menu
      EXIT MENU
  END MENU    
  

     
END MAIN