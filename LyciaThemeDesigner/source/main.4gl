##########################################################################
# LyciaThemeDesigner Project                                             #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

DEFINE f4,f5,f11,f12,f13,f15,f16,f21,f22 CHAR(100)
MAIN
OPEN WINDOW w WITH FORM "form" ATTRIBUTE(BORDER)
INPUT BY NAME f4,f5,f11,f12,f13,f15,f16,f21,f22
	BEFORE INPUT DISPLAY "*" TO bt23
	AFTER FIELD f4
		CALL checkFilds()
	AFTER FIELD f5
		CALL checkFilds()
	AFTER FIELD f11
		CALL checkFilds()
--	AFTER FIELD f12
--		CALL checkFilds()
	AFTER FIELD f13
		CALL checkFilds()
	AFTER FIELD f15
		CALL checkFilds()
	AFTER FIELD f16
		CALL checkFilds()
		NEXT FIELD f4
	ON ACTION actReg
		CALL fgl_winmessage("Registration","Data accepted. Registration successful.","info")
		EXIT INPUT
END INPUT
END MAIN

FUNCTION checkFilds()
MESSAGE ""
IF LENGTH(f4)=0 THEN
	MESSAGE "First Name field is empty!"
	DISPLAY "*" TO bt23
	NEXT FIELD f4
	RETURN
END IF
IF LENGTH(f5)=0 THEN
	MESSAGE "Last Name field is empty!"
	DISPLAY "*" TO bt23
	NEXT FIELD f5
	RETURN
END IF
IF LENGTH(f11)=0 THEN
	MESSAGE "Title field is empty!"
	DISPLAY "*" TO bt23
	NEXT FIELD f11
	RETURN
END IF
IF LENGTH(f12)=0 THEN
	MESSAGE "Title field is empty!"
	DISPLAY "*" TO bt23
	NEXT FIELD f12
	RETURN
END IF
IF LENGTH(f13)=0 THEN
	MESSAGE "Email field is empty!"
	DISPLAY "*" TO bt23
	NEXT FIELD f13
	RETURN
END IF

IF LENGTH(f15)=0 THEN
	MESSAGE "Code area field is empty!"
	DISPLAY "*" TO bt23
	NEXT FIELD f15
	RETURN
END IF
IF LENGTH(f16)=0 THEN
	MESSAGE "Phone field is empty!"
	DISPLAY "*" TO bt23
	NEXT FIELD f16
	RETURN
END IF
DISPLAY "!" TO bt23
END FUNCTION