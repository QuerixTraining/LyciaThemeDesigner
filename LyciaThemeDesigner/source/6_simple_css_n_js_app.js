/*
 LyciaThemeDesigner Demo Project
 Property of Querix Ltd.
 Copyright (C) 2016  Querix Ltd. All rights reserved.
 This program is free software: you can redistribute it. 
 You may modify this program only using Lycia.

 This program is distributed in the hope that it will be useful,
 but without any warranty; without even the implied warranty of
 merchantability or fitness for a particular purpose.

 Email: info@querix.com
*/
(function(querix){

    function installHandlers($) {
        $('body').on('dblclick','.qx-aum-text-field',function(e){
            var t = getTooltip(e);
            t.tooltip.css({
            	display: 'block',
                height: 'auto',
                left: t.tootipLeft + 'px',
                top: t.tooltipTop + 'px',
                overflow: 'visible'
            });
        });
        $('body').on('blur','.qx-aum-text-field',function(e){
            var t = getTooltip(e);
            t.tooltip.css({
                height: 0,
                left: '2500px',
                overflow: 'hidden'
            });
            setTimeout(function(){
	            t.tooltip.css({
	                display: 'none'
	            });
            },0);
        })
    }

    function getTooltip(e) {
        var target = $(e.target);
        var targetOffset = target.offset();
        var targetWidth = target.outerWidth();
        var targetHeight = target.outerHeight();
        var tooltip = $('.my-custom-tooltip');
        if (tooltip.length === 0) {
            tooltip = $('<div class="my-custom-tooltip">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>');
        }
        $('body').append(tooltip);
        tooltip.css({
            height: 'auto'
        });
        var tootipWidth = tooltip.outerWidth();
        var tootipHeight = tooltip.outerHeight();
        var tipWidth = 30;
        var tootipLeft = targetOffset.left + targetWidth + tipWidth;
        var tooltipTop = targetOffset.top + targetHeight/2 - tootipHeight/2;
        return {
            tooltip: tooltip,
            tootipLeft: tootipLeft,
            tooltipTop: tooltipTop
        };
    }

    installHandlers(window.top.$);
})(querix);
