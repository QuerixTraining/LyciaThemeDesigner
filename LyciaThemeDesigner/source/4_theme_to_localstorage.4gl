##########################################################################
# LyciaThemeDesigner Project                                             #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

main
 DEFINE
    buffer STRING,
    rf  SMALLINT,
    fullBuf string,
	res CHAR(100),
	clientFileName char(100)

	OPEN WINDOW w WITH FORM '4_theme_to_localstorage' ATTRIBUTE(BORDER)
	INPUT BY NAME clientFileName
	ON ACTION "Set to LocalStorage"
		CALL fgl_dialog_update_data()
		CALL fgl_upload(clientFileName, "theme_for_ls.qxtheme")
		CALL fgl_channel_open_file("stream", "theme_for_ls.qxtheme", "r")
		LET rf=true
		WHILE rf
  			LET fullBuf = fullBuf CLIPPED,buffer CLIPPED
  			LET rf = fgl_channel_read("stream", buffer) 
		END WHILE
		CALL ui.Interface.frontCall("html5", "setLocalStorage", ["ls_theme.qxtheme",fullBuf], [res])
		CALL fgl_rm ("theme_for_ls.qxtheme")		
		CALL fgl_winmessage("Set theme to LS","Theme is in LocalStorage","info")
	ON ACTION "EXIT"
		EXIT INPUT
 END INPUT
CLOSE WINDOW w
END MAIN